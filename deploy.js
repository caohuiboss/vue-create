/**
 * 部署之前请检查好要部署的路径
 * 如有多个环境，自行拓展
 */
const gulp = require('gulp')
const sftp = require('gulp-sftp-up5')
const CONFIG = require('./vue.config') // 只是为了保证上传的文件夹一致

const sftpConfig = {
// 此处的key对应着package.json中脚本的 APP_ENV
  test: {
    remotePath: '/service/web', // 部署到服务器的路径
    host: '192.168.0.99', // 服务器地址
    user: 'root', // 帐号
    pass: '1433223', // 密码
    port: 22, // 端口
    removeCurrentFolderFiles: true // 该属性可删除 remotePath 下的所有文件/文件夹
  },
  prod: {
    remotePath: '/service/web', // 部署到服务器的路径
    host: '127.0.0.1', // 服务器地址
    user: 'root', // 帐号
    pass: '1433223', // 密码
    port: 22, // 端口
    removeCurrentFolderFiles: true
  }
}

// 采用管道流的方式将 outputDir 中的文件上传到远端
gulp.src('./' + CONFIG.outputDir + '/**').pipe(sftp(sftpConfig[process.env.APP_ENV]))
